using Base
using FredholmIntegralProblems
using LinearAlgebra, SparseArrays, KrylovKit, ArnoldiMethod, WriteVTK
using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines


# define a geometry map and corresponding jacobian
F = stiffshell()
∇F = jacobian(F)

# S and S̃ must be defined on the same interval as F.space
S, S̃ = F.space, F.space

Δp = (0,0,0) .+ 0
Δp̃ = (0,0,0) .+ 0
Δr = (true, true, false)

# p-refinement
S = ⨷(map(s -> SplineSpace(s.p, copy(s.U)), S.data)...)
S̃ = ⨷(map(s -> SplineSpace(s.p, copy(s.U)), S̃.data)...)
S = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S.data, Δp))
S̃ = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S̃.data, Δp̃))

# enforce C^-1 at C^0 continuous patch boundaries
S̃ = ⨷(map((s,r) -> r ? roughen(s) : s, S̃.data, Δr))

# construct the insertion knots for Example 2-1
knot_vectors = map(s -> deconstruct_vector(s.U), S)
knots1 = (
    range(0.0, stop=0.25, length=22),
    range(0.25, stop=0.5, length=22),
    range(0.5, stop=0.75, length=22),
    range(0.75, stop=1.0, length=22) )
knots1 = vcat(map(x -> collect(x)[2:end-1], knots1)...)
knots2 = (
    range(knot_vectors[2][1][1], stop=knot_vectors[2][1][2], length=20),
    range(knot_vectors[2][1][2], stop=knot_vectors[2][1][3], length=3),
    range(knot_vectors[2][1][3], stop=knot_vectors[2][1][4], length=3)
)
knots2 = vcat(map(x -> collect(x)[2:end-1], knots2)...)
knots3 = [ 0.5 ]
insertion_knots = (knots1, knots2, knots3)
space_data = map(s -> (s.p, s.U), S)
map((data,knots) -> h_refinement_operator!(data[1],data[2],knots), space_data, insertion_knots)

# h-refinement of solution space
S = ⨷(map(data -> SplineSpace(data[1],data[2]), space_data)...)

# construct the insertion knots for Example 2-1
knot_vectors = map(s -> deconstruct_vector(s.U), S̃)
knots1 = (
    range(0.0, stop=0.25, length=22),
    range(0.25, stop=0.5, length=22),
    range(0.5, stop=0.75, length=22),
    range(0.75, stop=1.0, length=22) )
knots1 = vcat(map(x -> collect(x)[2:end-1], knots1)...)
knots2 = (
    range(knot_vectors[2][1][1], stop=knot_vectors[2][1][2], length=20),
    range(knot_vectors[2][1][2], stop=knot_vectors[2][1][3], length=3),
    range(knot_vectors[2][1][3], stop=knot_vectors[2][1][4], length=3)
)
knots2 = vcat(map(x -> collect(x)[2:end-1], knots2)...)
knots3 = [ 0.5 ]
insertion_knots = (knots1, knots2, knots3)
space_data = map(s -> (s.p, s.U), S̃)
map((data,knots) -> h_refinement_operator!(data[1],data[2],knots), space_data, insertion_knots)

# h-refinement of interpolation space
S̃ = ⨷(map(data -> SplineSpace(data[1],data[2]), space_data)...)

@show S, prod(map(dimsplinespace, S)), map(num_elements,S);
@show S̃, prod(map(dimsplinespace, S̃)), map(num_elements,S̃);

# get greville (interpolation) points
G = grevillepoints(S̃)

# in the case of C^-1 continous elements, deal with singular interpolation matrix
G = CartesianProduct(grevillepoints_singularityfix, G)

# compute square root of the determinant of the jacobian
J = sqrt.(geometric_factor(∇F(G)))

# generate a global quadrature
Q = QuadratureRule(TensorProduct(PatchRule, S))

# compute matrices
N, Ñ, M, B̃ₗ, B̃ᵤ, Zₗ, Zᵤ = setup(S,S̃,G,Q)

# define operators
σ, b, L = 1.0, 0.5, 176.21
y = F(G)
grid = (y...,y...)
K = KernelOperator(GaussianKernel(σ,b,L), grid)
L = LinearOperator(K,Zₗ,Zᵤ,M,B̃ₗ,B̃ᵤ,J)

# solve
nev = 20
@time decomp, history = ArnoldiMethod.partialschur(L; nev=nev, which=ArnoldiMethod.LM())
@time res = ArnoldiMethod.partialeigen(decomp)
@show history
lambda, v = res

# inverse eigenvectors transformation Lᵀv = v′
v = Zᵤ \ v

# postprocessing
lambda, v = reverse(lambda), reverse(v, dims=2)
@assert isapprox(norm(imag(lambda)), 0.0; atol=1e-12)
@assert isapprox(norm(imag(v)), 0.0; atol=1e-13)
trialspace = TensorProductBspline(S)
postprocessor = Postprocessor(F, trialspace; filename="examples/results_ex2-1_case_p2")
process_results!(postprocessor, lambda, v, 1:nev)
save_results(postprocessor)
    
