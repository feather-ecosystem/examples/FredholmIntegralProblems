using Base, Distributed, SlurmClusterManager
#addprocs(SlurmManager(), exeflags=["--project=@.", string("--threads=",ENV["SLURM_CPUS_PER_TASK"])])
#rmprocs(nworkers())
#addprocs(2, exeflags=["--project=@.", string("--threads=4")])

@everywhere begin 
    using FredholmIntegralProblems
    using LinearAlgebra, SparseArrays, KrylovKit, ArnoldiMethod, WriteVTK
    using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines
    using Statistics
end

# define a geometry map and corresponding jacobian
F = partial_tube(;inner_radius=8.0, outer_radius=10.0, height=15.0,  β=π)
∇F = jacobian(F)

# S and S̃ must be defined on the same interval as F.space
S, S̃ = F.space, F.space

Δp = (1,0,1) .+ 0
Δh = (4,30,30) .÷ 1

Δp̃ = (1,0,1) .+ 2
Δh̃ = (4,30,30) .÷ 1

Δr = (false, true, false)

# p-refinement
S = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S.data, Δp))
S̃ = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S̃.data, Δp̃))

# enforce C^-1 at C^0 continuous patch boundaries
S̃ = ⨷(map((s,r) -> r ? roughen(s) : s, S̃.data, Δr))

# h-refinement
S = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S.data, Δh))
S̃ = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S̃.data, Δh̃))

println("Domain info:")
@show S, prod(map(dimsplinespace, S)), map(num_elements,S)
@show S̃, prod(map(dimsplinespace, S̃)), map(num_elements,S̃)
print("\n\n")

# get greville (interpolation) points
G = grevillepoints(S̃)

# in the case of C^-1 continous elements, deal with singular interpolation matrix
G = CartesianProduct(grevillepoints_singularityfix, G)

# compute square root of the determinant of the jacobian
J = sqrt.(geometric_factor(∇F(G)))

# generate a global quadrature
Q = QuadratureRule(TensorProduct(PatchRule, S))

# compute matrices
N = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S, Q.x, reverse=true)
Ñ = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S̃, Q.x, reverse=true)

M = KroneckerProduct(massmatrix, Ñ.data, N.data, reverse(Q.w.data))

B̃ = map((S̃ₖ,Gₖ) -> lu(Matrix(bspline_interpolation_matrix(S̃ₖ, Gₖ)), Val(false)), S̃, G)
B̃ₗ = KroneckerProduct(B̃ₖ -> B̃ₖ.L, B̃, reverse=true)
B̃ᵤ = KroneckerProduct(B̃ₖ -> B̃ₖ.U, B̃, reverse=true)

# using Symmetric() because Julia and cholesky are allergic to numeric asymmetries...
Z = cholesky(KroneckerProduct(Symmetric ∘ massmatrix, N.data, N.data, reverse(Q.w.data), reverse=true))
Zₗ = KroneckerProduct(Matrix, Z.L.data, reverse=true)
Zᵤ = KroneckerProduct(Matrix, Z.U.data, reverse=true)

# setup
σ, b, L = 1.0, 0.5, 10.0
y = F(G)
grid = (y...,y...)

# define operators
K = KernelOperator(GaussianKernel(σ,b,L), grid)
L = LinearOperator(K,Zₗ,Zᵤ,M,B̃ₗ,B̃ᵤ,J)

# run once and test
b = zeros(size(K,1)); v = rand(size(K,1));
println("Precompiling and single run...")
@time FredholmIntegralProblems.mul(K,v);
@time FredholmIntegralProblems.mul(K,v);
@time FredholmIntegralProblems.mul(K,v);
print("\n\n")

# statistics for step 5
nsample = 5
println("Step 5:")
b = zeros(size(K,1)); v = rand(size(K,1));
res = [@elapsed FredholmIntegralProblems.mul(K,v) for k in Base.OneTo(nsample)];
println("avg: ", mean(res))
println("std: ", std(res))
println("min: ", minimum(res))
println("max: ", maximum(res))
print("\n\n")

println("Workers:")
id, host, nthreads = (myid(), gethostname(), Threads.nthreads())
println("Root ", id, " @ " , host, ", nthreads: ", nthreads)
for i in workers()
    wid, whost, wnthreads = fetch(@spawnat i (myid(), gethostname(), Threads.nthreads()))
    println("Worker ", wid, " @ " , whost, ", nthreads: ", wnthreads)
end
