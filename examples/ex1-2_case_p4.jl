using Base
using FredholmIntegralProblems
using LinearAlgebra, SparseArrays, Arpack, KrylovKit, ArnoldiMethod, WriteVTK
using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines

# define a geometry map and corresponding jacobian
F = partial_tube(;inner_radius=8.0, outer_radius=10.0, height=15.0,  β=π)
∇F = jacobian(F)

# S and S̃ must be defined on the same interval as F.space
S, S̃ = F.space, F.space

#Δp = (1,0,1) .+ 1
#Δh = (4,30,30) .÷ 0.25
#
#Δp̃ = (1,0,1) .+ 2
#Δh̃ = (4,30,30) .÷ 0.25

Δp = (1,0,1) .+ 0
Δh = (0,15,7) .* 1

Δp̃ = (1,0,1) .+ 2
Δh̃ = (0,15,7) .* 1

Δr = (false, true, false)

Δr = (false, true, false)

# p-refinement
S = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S.data, Δp))
S̃ = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S̃.data, Δp̃))

# enforce C^-1 at C^0 continuous patch boundaries
S̃ = ⨷(map((s,r) -> r ? roughen(s) : s, S̃.data, Δr))

# h-refinement
S = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S.data, Δh))
S̃ = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S̃.data, Δh̃))

@show S, prod(map(dimsplinespace, S)), map(num_elements,S)
@show S̃, prod(map(dimsplinespace, S̃)), map(num_elements,S̃)

# get greville (interpolation) points
G = grevillepoints(S̃)

# in the case of C^-1 continous elements, deal with singular interpolation matrix
G = CartesianProduct(grevillepoints_singularityfix, G)

# compute square root of the determinant of the jacobian
J = sqrt.(geometric_factor(∇F(G)))

# generate a global quadrature
Q = QuadratureRule(TensorProduct(PatchRule, S))

# compute matrices
N, Ñ, M, B̃ₗ, B̃ᵤ, Zₗ, Zᵤ = setup(S,S̃,G,Q)

# define operators
σ, b, L = 1.0, 0.5, 10.0
y = F(G)
grid = (y...,y...)
K = KernelOperator(GaussianKernel(σ,b,L), grid)
L = LinearOperator(K,Zₗ,Zᵤ,M,B̃ₗ,B̃ᵤ,J)

# solve
nev = 20
@time decomp, history = ArnoldiMethod.partialschur(L; nev=nev, which=ArnoldiMethod.LM())
@time res = ArnoldiMethod.partialeigen(decomp)
@show history
lambda, v′ = res

#@time Arpack.eigs(L; nev=20, which=:LM, ritzvec=false, explicittransform=:auto)
#@time KrylovKit.eigsolve(L, size(L,1), 20, :LM; issymmetric=true, ishermitian=true)

# inverse eigenvectors transformation Lᵀv = v′
v = Zᵤ \ v′

# postprocessing
lambda, v = reverse(lambda), reverse(v, dims=2)
trialspace = TensorProductBspline(S)
postprocessor = Postprocessor(F, trialspace; filename="examples/results_ex1-2_case_p4", density=50)
process_results!(postprocessor, lambda, v, 1:nev)
save_results(postprocessor)

@show lambda


