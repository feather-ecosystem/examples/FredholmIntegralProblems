using Base
using FredholmIntegralProblems
using LinearAlgebra, SparseArrays, KrylovKit, ArnoldiMethod, WriteVTK, LinearMaps, Arpack
using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines


# Example 1-2 Case Galerkin 1 but with Bsplines!
F = partial_tube(;inner_radius=8.0, outer_radius=10.0, height=15.0,  β=π)
∇F = jacobian(F)

# solution space
S = F.space
Δp = (1,0,1) .+ 0
Δh = (0,15,7) .* 1

# p-refinement
S = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S.data, Δp))

# h-refinement
S = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S.data, Δh))

@show S, prod(map(dimsplinespace, S)), map(num_elements,S)

# generate a global quadrature
Q = QuadratureRule(TensorProduct(PatchRule, S))

# compute determinant of the jacobian
J = geometric_factor(∇F(Q.x))

# compute matrices
Bi = KroneckerProduct((Sₖ,Qₖ,wₖ) -> weight_bspline_interpolation_matrix(bspline_interpolation_matrix(Sₖ,Qₖ), wₖ), S, Q.x, Q.w.data, reverse=true)
Bi = Matrix(Bi) .* view(J,:)
Bj = Matrix(KroneckerProduct(bspline_interpolation_matrix, S, Q.x, reverse=true))

# define operators
σ, b, L = 1.0, 0.5, 10.0
y = F(Q.x)
grid = (y...,y...)
K = KernelOperator(ExponentialKernel(σ,b,L), grid)

# assemble (naive, memory intensive but fast)
G = Matrix(K)
A = Bi' * G * Bi
Z = Bi' * Bj

# solve
nev = 20
lambda, v = Arpack.eigs(A,Z; nev)

# postprocessing
@assert isapprox(norm(imag(lambda)), 0.0; rtol=eps(Float64))
@assert isapprox(norm(imag(v)), 0.0; rtol=eps(Float64))
lambda, v = real(lambda), real(v)
trialspace = TensorProductBspline(S)
postprocessor = Postprocessor(F, trialspace; filename="examples/results_ex1-2_case_Galerkin1", density=50)
process_galerkin_results!(postprocessor, lambda, v, 1:nev)
save_results(postprocessor)

@show lambda