figure

p = [24, 48, 96, 2*96, 4*96, 8*96, 16*96]
t = [6788.08, 3433.331, 1876.9824, 932.81, 472.16567, 242.8768, 136.52]./60

hold on
p1 = loglog(p,p(1)./p*t(1))
p2 = loglog(p,p(3)./p*t(3))
p3 = loglog(p,t)
hold off

grid on
title("Strong scaling on Lichtenberg cluster for N_{proc} = [24, 48, 96, 192, 384, 768, 1536]")
set(gca, "fontsize", 8, "fontname", "Roboto Slab")
color = 150
set(p1,"linewidth",1,"linestyle",":","color",[color,color,color]./256)
set(p2,"linewidth",1,"linestyle",":","color",[color,color,color]./256)
set(p3,"linewidth",1,"linestyle","-.","color","black","marker","x","markersize",3.5)
xlabel("N_{proc}")
ylabel("Avg. T(N_{proc}) [min/mvp]")
legend([p1,p3],"O(N^{-1}_{proc})","2.6M DOF","location","southwest")
print(gcf, "plot1.png", "-r600")
