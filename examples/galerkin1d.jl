using Base, Plots
using FredholmIntegralProblems
using LinearAlgebra, SparseArrays, KrylovKit, ArnoldiMethod, WriteVTK, LinearMaps, Arpack
using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines

L = 1.
p = Degree(2)
U = KnotVector([0.0,L], [p+1,p+1])
S = SplineSpace(p, U)

Δp = 0
Δh = 200

# p-refinement
S = Δp > 0 ? refine(S, pRefinement(Δp)) : S
S = Δh > 0 ? refine(S, hRefinement(Δh)) : S

@show S, dimsplinespace(S), num_elements(S)

# generate a global quadrature
Q = PatchRule(S)

# compute matrices
Bi = weight_bspline_interpolation_matrix(bspline_interpolation_matrix(S,Q.x), Q.w)
Bj = bspline_interpolation_matrix(S, Q.x)

# define operators
σ, b, L = 1.0, 1.0, 1.0
pointsgrid = (Array(Q.x),Array(Q.x))
K = KernelOperator(SeparableKernel(σ,b,L), pointsgrid)

# assemble
G = Matrix(K)
A = Bi' * G * Bi
Z = Bi' * Bj

# solve
nev = 20
lambda, v = Arpack.eigs(A,Z; nev)

# postprocessing
@assert isapprox(norm(imag(lambda)), 0.0; rtol=eps(Float64))
@assert isapprox(norm(imag(v)), 0.0; rtol=eps(Float64))
lambda, v = real(lambda), real(v)
trialspace = Bspline(S)

plt = plot()
linestyle = [:solid, :dot, :dash, :dashdot]
signsflip = [-1, 1, 1, 1]
for k in 1:4
    trialspace.coeffs[:] .= v[:,k]
    β = sqrt(sum(Q.w .* trialspace(Q.x).^2)) # ∫ϕ²dx == 1
    trialspace.coeffs[:] .= signsflip[k]/β * v[:,k]
    plot!(plt, trialspace, linestyle=linestyle[k])
end
plot!(plt, xlabel="x", ylabel="ϕ", legend=false)
display(plt) # compare Ghanem and Spanos (1991), Fig. 2.1

@show lambda
