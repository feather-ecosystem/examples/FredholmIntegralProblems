using Base
using FredholmIntegralProblems
using LinearAlgebra, SparseArrays, KrylovKit, ArnoldiMethod, WriteVTK, LinearMaps, Arpack
using UnivariateSplines, NURBS, CartesianProducts, SortedSequences, KroneckerProducts, FeaInterfaces, TensorProductBsplines


function run(Δp, Δp̃, Δh, Δh̃, Δr)
    F = cube(;width=1,height=1,depth=1)
    ∇F = jacobian(F)
    
    # S and S̃ must be defined on the same interval as F.space
    S, S̃ = F.space, F.space
    
    # p-refinement
    S = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S.data, Δp))
    S̃ = ⨷(map((s,dp) -> dp > 0 ? refine(s,pRefinement(dp)) : s, S̃.data, Δp̃))
    
    # enforce C^-1 at C^0 continuous patch boundaries
    S̃ = ⨷(map((s,r) -> r ? roughen(s) : s, S̃.data, Δr))
    
    # h-refinement
    S = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S.data, Δh))
    S̃ = ⨷(map((s,dh) -> dh > 0 ? refine(s,hRefinement(dh)) : s, S̃.data, Δh̃))
    
    @show S, prod(map(dimsplinespace, S)), map(num_elements,S)
    @show S̃, prod(map(dimsplinespace, S̃)), map(num_elements,S̃)
    
    # get greville (interpolation) points
    G = grevillepoints(S̃)
    
    # in the case of C^-1 continous elements, deal with singular interpolation matrix
    G = CartesianProduct(grevillepoints_singularityfix, G)
    
    # compute square root of the determinant of the jacobian
    J = sqrt.(geometric_factor(∇F(G)))
    
    # generate a global quadrature
    Q = QuadratureRule(TensorProduct(PatchRule, S))
    
    # compute matrices
    N = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S, Q.x, reverse=true)
    Ñ = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S̃, Q.x, reverse=true)
    
    M = KroneckerProduct(massmatrix, Ñ.data, N.data, reverse(Q.w.data))
    
    B̃ = map((S̃ₖ,Gₖ) -> lu(Matrix(bspline_interpolation_matrix(S̃ₖ, Gₖ)), Val(false)), S̃, G)
    B̃ₗ = KroneckerProduct(B̃ₖ -> B̃ₖ.L, B̃, reverse=true)
    B̃ᵤ = KroneckerProduct(B̃ₖ -> B̃ₖ.U, B̃, reverse=true)
    
    # using Symmetric() because Julia and cholesky are allergic to numeric asymmetries...
    Z = cholesky(KroneckerProduct(Symmetric ∘ massmatrix, N.data, N.data, reverse(Q.w.data), reverse=true))
    Zₗ = KroneckerProduct(Matrix, Z.L.data, reverse=true)
    Zᵤ = KroneckerProduct(Matrix, Z.U.data, reverse=true)
    
    # define operators
    σ, b, L = 1.0, 1.0, 1.0
    y = F(G)
    grid = (y...,y...)
    K = KernelOperator(SeparableKernel(σ,b,L), grid)
    L = LinearOperator(K,Zₗ,Zᵤ,M,B̃ₗ,B̃ᵤ,J)
    
    # solve
    nev = 20
    @time res = ArnoldiMethod.partialeigen(ArnoldiMethod.partialschur(L; nev=nev, which=ArnoldiMethod.LM())[1])
    lambda, v′ = res
    
    ## inverse eigenvectors transformation Lᵀv = v′
    v = Zᵤ \ v′
    
    ## postprocessing
    lambda, v = reverse(lambda), reverse(v, dims=2)
    trialspace = TensorProductBspline(S)
    postprocessor = Postprocessor(F, trialspace; filename="examples/results_cube")
    process_results!(postprocessor, lambda, v, 1:nev)
    save_results(postprocessor)
    
    @show lambda
end

Δp = (1,1,1) .+ 0
Δh = (1,1,1) .* 10

Δp̃ = (1,1,1) .+ 0
Δh̃ = (1,1,1) .* 10

Δr = (false, false, false)

run( (1,1,1), (1,1,1), (1,1,1), (1,1,1), (false, false, false) ) ## trigger compilation
run(Δp, Δp̃, Δh, Δh̃, Δr) ## run