using Documenter, FredholmIntegralProblems

makedocs(
    modules  = [FredholmIntegralProblems],
    sitename = "FredholmIntegralProblems.jl",
    authors  = "Michał Ł. Mika",
    pages    = [
                    "Home"  =>  "index.md"
               ],
    repo = "https://gitlab.com/feather-ecosystem/FredholmIntegralProblems"
)
