#!/bin/bash

#SBATCH -J FredholmIntegralProblem
#SBATCH --mail-type=ALL
#SBATCH -e /work/scratch/${USER}/FredholmIntegralProblem/log/%x.err.%j
#SBATCH -o /work/scratch/${USER}/FredholmIntegralProblem/log/%x.out.%j
#SBATCH --mem-per-cpu=3800
#SBATCH -t 00:30:00
#SBATCH --cpus-per-task=96
#SBATCH --ntasks=2
#SBATCH --nodes=2

export PROJECT_DIRECTORY=$HPC_SCRATCH/FredholmIntegralProblem
export JULIA_DEPOT_PATH=${PROJECT_DIRECTORY}/depot

cd ${PROJECT_DIRECTORY}/code
../julia-1.7.3/bin/julia -t $SLURM_CPUS_PER_TASK --project=@. examples/statistics.jl
