# FredholmIntegralProblems

[![pipeline status](https://gitlab.com/feather-ecosystem/FredholmIntegralProblems/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/FredholmIntegralProblems/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/FredholmIntegralProblems/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/FredholmIntegralProblems/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:examples/FredholmIntegralProblems/branch/master/graph/badge.svg?token=QG75FOD9OI)](https://codecov.io/gl/feather-ecosystem:examples/FredholmIntegralProblems)

Julia implementation of the method described in

M.L. Mika, T.J.R. Hughes, D. Schillinger, P. Wriggers and R.R. Hiemstra, _A matrix-free isogeometric Galerkin method for Karhunen-Loève approximation of random fields using tensor product splines, tensor contraction and interpolation based quadrature_, Computer Methods in Applied Mechanics and Engineering, Volume 379, 2021. [https://doi.org/10.1016/j.cma.2021.113730](https://doi.org/10.1016/j.cma.2021.113730)

<div align="center"><img src="https://mika.sh/images/20200706_img2.png"  width="56%"><img src="https://mika.sh/images/20200706_img3.png"  width="35%"></div>


## Lichtenberg cluster setup

Create a project directory in `scratch`
```
[${USER}@logc0002 ~]$ mkdir /work/scratch/${USER}/FredholmIntegralProblems/
```

The project directory structure will eventually be
```
[${USER}@logc0002 FredholmIntegralProblems]$ tree -L 1 /work/scratch/${USER}/FredholmIntegralProblems/
/work/scratch/${USER}/FredholmIntegralProblems/
`-- code
|-- depot
|-- julia-1.7.3
`-- log

4 directories, 0 files
```

The directory `code` holds the repository. Clone it inside the project directory
```
[${USER}@logc0002 ~]$ cd /work/scratch/${USER}/FredholmIntegralProblems/
[${USER}@logc0002 FredholmIntegralProblems]$ pwd
/work/scratch/${USER}/FredholmIntegralProblems
[${USER}@logc0002 FredholmIntegralProblems]$ git clone --branch master https://gitlab.com/feather-ecosystem/examples/FredholmIntegralProblems.git code
[${USER}@logc0002 FredholmIntegralProblems]$ ls code
LICENSE  Project.toml  README.md  docs  examples  slurm.sh  src  test
```

Download Julia and extract it
```
[${USER}@logc0002 FredholmIntegralProblems]$ wget https://julialang-s3.julialang.org/bin/linux/x64/1.7/julia-1.7.3-linux-x86_64.tar.gz
--2022-08-01 13:26:34--  https://julialang-s3.julialang.org/bin/linux/x64/1.7/julia-1.7.3-linux-x86_64.tar.gz
Resolving julialang-s3.julialang.org (julialang-s3.julialang.org)... 2a04:4e42:3::561, 151.101.14.49
Connecting to julialang-s3.julialang.org (julialang-s3.julialang.org)|2a04:4e42:3::561|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 123160673 (117M) [application/x-tar]
Saving to: 'julia-1.7.3-linux-x86_64.tar.gz'

julia-1.7.3-linux-x86_64.tar.gz                           100%[=============>] 117.46M   327MB/s    in 0.4s

2022-08-01 13:26:35 (327 MB/s) - 'julia-1.7.3-linux-x86_64.tar.gz' saved [123160673/123160673]
[${USER}@logc0002 FredholmIntegralProblems]$ tar xzvf julia-1.7.3-linux-x86_64.tar.gz
```

Create `log` and `depot` directory
```
[${USER}@logc0002 FredholmIntegralProblems]$ mkdir log
[${USER}@logc0002 FredholmIntegralProblems]$ mkdir depot
[${USER}@logc0002 FredholmIntegralProblems]$ ls
code  depot  julia-1.7.3  julia-1.7.3-linux-x86_64.tar.gz  log
```

Run interactively on a login node to test and instantiate
```
[${USER}@logc0002 code]$ JULIA_DEPOT_PATH=/work/scratch/${USER}/FredholmIntegralProblems/depot ../julia-1.7.3/bin/julia --project=@.
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.7.3 (2022-05-06)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

julia> @show ENV["JULIA_DEPOT_PATH"]
ENV["JULIA_DEPOT_PATH"] = "/work/scratch/${USER}/FredholmIntegralProblems/depot"
"/work/scratch/${USER}/FredholmIntegralProblems/depot"

(FredholmIntegralProblems) pkg> registry add https://gitlab.com/feather-ecosystem/FeatherRegistry.git
     Cloning registry from "https://gitlab.com/feather-ecosystem/FeatherRegistry.git"
       Added registry `FeatherRegistry` to `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/FeatherRegistry`

(FredholmIntegralProblems) pkg> registry update
    Updating registry at `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/FeatherRegistry`
    Updating git-repo `https://gitlab.com/feather-ecosystem/FeatherRegistry.git`
    Updating registry at `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/General.toml`

(FredholmIntegralProblems) pkg> registry add https://github.com/JuliaRegistries/General
     Cloning registry from "https://github.com/JuliaRegistries/General"
       Added registry `General` to `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/General`

(FredholmIntegralProblems) pkg> instantiate
    Updating registry at `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/FeatherRegistry`
    Updating git-repo `https://gitlab.com/feather-ecosystem/FeatherRegistry.git`
    Updating registry at `/work/scratch/${USER}/FredholmIntegralProblems/depot/registries/General.toml`
   Installed JpegTurbo_jll ──────────────────── v2.1.2+0
   Installed x265_jll ───────────────────────── v3.5.0+0
   Installed Calculus ───────────────────────── v0.5.1
   Installed libfdk_aac_jll ─────────────────── v2.0.2+0
   Installed iso_codes_jll ──────────────────── v4.10.0+0
   Installed LRUCache ───────────────────────── v1.3.0
...
  [8e850b90] + libblastrampoline_jll
  [8e850ede] + nghttp2_jll
  [3f19e933] + p7zip_jll
    Building Conda ─→ `/work/scratch/${USER}/FredholmIntegralProblems/depot/scratchspaces/44cfe95a-1eb2-52ea-b672-e2afdf69b78f/6e47d11ea2776bc5627421d59cdcc1296c058071/build.log`
    Building PyCall → `/work/scratch/${USER}/FredholmIntegralProblems/depot/scratchspaces/44cfe95a-1eb2-52ea-b672-e2afdf69b78f/1fc929f47d7c151c839c5fc1375929766fb8edcc/build.log`
    Building GR ────→ `/work/scratch/${USER}/FredholmIntegralProblems/depot/scratchspaces/44cfe95a-1eb2-52ea-b672-e2afdf69b78f/037a1ca47e8a5989cc07d19729567bb71bfabd0c/build.log`
    Building HDF5 ──→ `/work/scratch/${USER}/FredholmIntegralProblems/depot/scratchspaces/44cfe95a-1eb2-52ea-b672-e2afdf69b78f/9ffc57b9bb643bf3fce34f3daf9ff506ed2d8b7a/build.log`

julia> exit()
```

Run `examples/statistics.jl` with 12 threads on 1 (login) node.
```
[${USER}@logc0002 code]$ JULIA_DEPOT_PATH=/work/scratch/${USER}/FredholmIntegralProblems/depot ../julia-1.7.3/bin/julia -t 12 --project=@. examples/statistics.jl
WARNING: could not import HDF5.exists into MAT
┌ Warning: Type annotations on keyword arguments not currently supported in recipes. Type information has been discarded
└ @ RecipesBase /work/scratch/${USER}/FredholmIntegralProblems/depot/packages/RecipesBase/qpxEX/src/RecipesBase.jl:123
Domain info:
(S, prod(map(dimsplinespace, S)), map(num_elements, S)) = (TensorProduct{3, SplineSpace{Float64}}((SplineSpace(degree = 2, interval = [0.0, 2.0], dimension = 5), SplineSpace(degree = 2, interval = [0.0, 3.141592653589793], dimension = 15), SplineSpace(degree = 2, interval = [0.0, 15.0], dimension = 8))), 600, [3, 12, 6])
(S̃, prod(map(dimsplinespace, S̃)), map(num_elements, S̃)) = (TensorProduct{3, SplineSpace{Float64}}((SplineSpace(degree = 4, interval = [0.0, 2.0], dimension = 7), SplineSpace(degree = 4, interval = [0.0, 3.141592653589793], dimension = 20), SplineSpace(degree = 4, interval = [0.0, 15.0], dimension = 10))), 1400, [3, 12, 6])


Precompiling and single run...
  1.772705 seconds (6.52 M allocations: 355.044 MiB, 8.74% gc time, 99.77% compilation time)
  0.003078 seconds (1.98 k allocations: 208.375 KiB)
  0.003057 seconds (1.98 k allocations: 208.406 KiB)


Step 5:
avg: 0.002955148
std: 0.0001568719642766035
min: 0.002849517
max: 0.003232499


Workers:
Root 1 @ logc0002, nthreads: 12
Worker 1 @ logc0002, nthreads: 12
```

On login nodes you should compute only small test cases. Do not use all available resources there! To run on one or multiple MPI nodes submit the SLURM job `slurm.sh`. Review `slurm.sh` and change according to your needs i.e. the `${USER}`, all paths and required resources. Uncomment the `SlurmManager` in `examples/statistics.jl`!
```
[${USER}@logc0002 code]$ sbatch slurm.sh
Submitted batch job 29793846
[I] Possible CPU types    = (not limited)
[I] Possible specials     = short
[${USER}@logc0002 code]$ squeue
     JOBID PARTITION     NAME     USER    STATE       TIME TIME_LIMIT PRIORITY    NODES NODELIST(REASON)
  29793846 deflt_sho Fredholm  ${USER}  RUNNING       0:03      30:00 1370622         2 mpsc[0001-0002]
[${USER}@logc0002 code]$ cat ../log/FredholmIntegralProblem.out.29793846
Domain info:
(S, prod(map(dimsplinespace, S)), map(num_elements, S)) = (TensorProduct{3, SplineSpace{Float64}}((SplineSpace(degree = 2, interval = [0.0, 2.0], dimension = 5), SplineSpace(degree = 2, interval = [0.0, 3.141592653589793], dimension = 15), SplineSpace(degree = 2, interval = [0.0, 15.0], dimension = 8))), 600, [3, 12, 6])
(S̃, prod(map(dimsplinespace, S̃)), map(num_elements, S̃)) = (TensorProduct{3, SplineSpace{Float64}}((SplineSpace(degree = 4, interval = [0.0, 2.0], dimension = 7), SplineSpace(degree = 4, interval = [0.0, 3.141592653589793], dimension = 20), SplineSpace(degree = 4, interval = [0.0, 15.0], dimension = 10))), 1400, [3, 12, 6])


Precompiling and single run...
  3.012894 seconds (6.52 M allocations: 355.516 MiB, 4.20% gc time, 53.38% compilation time)
  0.025874 seconds (230 allocations: 186.047 KiB)
  0.021921 seconds (233 allocations: 186.625 KiB)


Step 5:
avg: 0.007947174800000002
std: 0.006012773528799185
min: 0.004109557
max: 0.018502383


Workers:
Root 1 @ mpsc0001, nthreads: 96
Worker 2 @ mpsc0001, nthreads: 96
Worker 3 @ mpsc0002, nthreads: 96
```
