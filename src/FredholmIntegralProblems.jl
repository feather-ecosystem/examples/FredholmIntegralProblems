
module FredholmIntegralProblems

import Base.:*

using Base: Float64
using LinearAlgebra
using Folds
using SortedSequences
using CartesianProducts
using FeaInterfaces
using KroneckerProducts
using LinearMaps
using TensorOperations
using WriteVTK
using KrylovKit
using UnivariateSplines
using NURBS
using KroneckerProducts
using TensorProductBsplines
using DelimitedFiles
using Distributed
using StaticArrays
using LoopVectorization

include("base.jl")
include("assembly.jl")
include("kernels.jl")
include("linearoperator.jl")
include("postprocessor.jl")

end # module
