export massmatrix, weight_bspline_interpolation_matrix, setup

function massmatrix(N::AbstractMatrix{T}, M::AbstractMatrix{T}, w::AbstractVector{T}) where T
    return N' * (M .* w)
end

function weight_bspline_interpolation_matrix(N::AbstractMatrix{T}, w::AbstractVector{T}) where T
    return N .* w
end

function setup(S,S̃,G,Q)
    N = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S, Q.x, reverse=true)
    Ñ = KroneckerProduct(Matrix ∘ bspline_interpolation_matrix, S̃, Q.x, reverse=true)

    M = KroneckerProduct(massmatrix, Ñ.data, N.data, reverse(Q.w.data))

    B̃ = map((S̃ₖ,Gₖ) -> lu(Matrix(bspline_interpolation_matrix(S̃ₖ, Gₖ)), Val(false)), S̃, G)
    B̃ₗ = KroneckerProduct(B̃ₖ -> B̃ₖ.L, B̃, reverse=true)
    B̃ᵤ = KroneckerProduct(B̃ₖ -> B̃ₖ.U, B̃, reverse=true)

    # using Symmetric() because Julia and cholesky are allergic to numeric asymmetries...
    Z = cholesky(KroneckerProduct(Symmetric ∘ massmatrix, N.data, N.data, reverse(Q.w.data), reverse=true))
    Zₗ = KroneckerProduct(Matrix, Z.L.data, reverse=true)
    Zᵤ = KroneckerProduct(Matrix, Z.U.data, reverse=true)
    
    return N, Ñ, M, B̃ₗ, B̃ᵤ, Zₗ, Zᵤ
end