export KernelOperator, GaussianKernel, ExponentialKernel, SeparableKernel, Matrix, size, mul, mul!, diffnorm!, rowdot!

macro viewpoints(grid, k, l)
    return :( ( view(getindex($(esc(grid)),1), :, $(esc(k))), view(getindex($(esc(grid)),2), :, $(esc(l))) ) )
end

macro viewpoint(grid, dir, k)
    return :( view(getindex($(esc(grid)),$(esc(dir))), :, $(esc(k))) )
end

@inline function diffsquare!(v::MVector{N,T}, x::S, y::S) where {N,T,S}
    v .= (x .- y).^2
end

@inline function diffabs!(v::MVector{N,T}, x::S, y::S) where {N,T,S}
    v .= abs.(x .- y)
end

abstract type AbstractKernel end 

struct GaussianKernel{T} <: AbstractKernel
    σ::T
    b::T
    L::T
end

struct ExponentialKernel{T} <: AbstractKernel
    σ::T
    b::T
    L::T
end

struct SeparableKernel{T} <: AbstractKernel
    σ::T
    b::T
    L::T
end

@inline function (f::GaussianKernel)(v::MVector{N,T}, x::S, y::S) where {N,T,S}
    diffsquare!(v, x, y)
    return f.σ^2 * exp( -sqrt(sum(v))^2 / (f.b * f.L)^2 )
end

@inline function (f::ExponentialKernel)(v::MVector{N,T}, x::S, y::S) where {N,T,S}
    diffsquare!(v, x, y)
    return f.σ^2 * exp( -sqrt(sum(v)) / (f.b * f.L) )
end

@inline function (f::SeparableKernel)(v::MVector{N,T}, x::S, y::S) where {N,T,S}
    diffabs!(v, x, y)
    return f.σ^2 * exp( -sum(v) / (f.b * f.L) )
end

struct KernelOperator{I<:AbstractKernel,T,D,N} <: AbstractMatrix{T}
    kernel::I
    grid::NTuple{2,Matrix{T}}
    size::NTuple{2,Int64}
    partition::Base.Iterators.PartitionIterator{UnitRange{Int64}}
end

function KernelOperator(kernel::I, grid::NTuple{D,Array{T,N}}) where {I<:AbstractKernel,T,D,N}
    vecgrid = map(x->x[:], grid)

    X = vcat(map(transpose, vecgrid[1:N])...)
    Y = vcat(map(transpose, vecgrid[N+1:D])...)
    
    n, m = map(d -> size(d,2), (X,Y))
    nworkers = Distributed.nworkers()
    chunksize = (m ÷ nworkers) + 1
    partition = Iterators.partition(1:m, chunksize)
    
    KernelOperator{I,T,D,N}(kernel, (X,Y), (n,m), partition)
end

Base.size(K::KernelOperator) = K.size

function rowdot(K::KernelOperator{I,T,D,N}, v::AbstractVector{T}, row::Int64) where {I,T,D,N}
    m, n = size(K)
    d = @MVector zeros(N)

    res = 0
    x = @viewpoint K.grid 1 row
    @inbounds for k in Base.OneTo(n)
        y = @viewpoint K.grid 2 k
        res += v[k] * K.kernel(d, x, y)
    end
    
    return res
end

function rowsdot(K::KernelOperator{I,T,D,N}, v::AbstractVector{T}, chunk::UnitRange{Int64}) where {I,T,D,N}
    m, n = size(K)
    b = zeros(length(chunk))
    
    if Threads.nthreads() > 1
        Folds.map!(row -> rowdot(K,v,row), b, chunk, ThreadedEx(;basesize=10))
    else
        d = @MVector zeros(N)
        @inbounds for row in chunk
            @inbounds for k in Base.OneTo(n)
                x, y = @viewpoints K.grid row k
                b[row] += v[k] * K.kernel(d, x, y)
            end
        end
    end
    return b
end

function mul(K::KernelOperator{I,T,D,N}, v::AbstractVector{T}) where {I,T,D,N}
    m, n = size(K)
    @assert(n == length(v))

    B = pmap(chunk->rowsdot(K,v,chunk), K.partition)
    b = vcat(B...)
    return b
end

# code generation for (mutable) static vector
for DIM in 1:3

    # just for *printing* of covariance matrix coefficient (no need for optimization)
    @eval function Base.getindex(K::KernelOperator{I,T,D,$DIM}, linds...) where {I,T,D}
        k, l = linds
        d = @MVector zeros($DIM)
        x, y = @viewpoints K.grid k l
        return K.kernel(d, x, y)
    end

    # just for materializing of small covariance matrices (no need for optimization)
    @eval function Base.Matrix(K::KernelOperator{I,T,D,$DIM}) where {I,T,D}
        m, n = size(K)
        d = @MVector zeros($DIM)
        res = Matrix{Float64}(undef, m, n)
        
        @inbounds for l in Base.OneTo(n)
            @inbounds for k in Base.OneTo(m)
                x, y = @viewpoints K.grid k l
                res[k,l] = K.kernel(d, x, y)
            end
        end
        return res
    end

end