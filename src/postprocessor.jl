export vtk_add_field!, vtk_initialize_model, Postprocessor, process_results!, process_galerkin_results!, save_results

struct Postprocessor
    vtkmodel
    trialspace
    J
    grid
end

function Postprocessor(F, trialspace; filename="results", density=nothing)
    if density == nothing
        grid = CartesianProduct(map(s -> IncreasingVector(unique(s.U)), trialspace.space.data)...)
    else
        grid = CartesianProduct(map(s -> IncreasingRange(s.U[1], s.U[end], density), F.space)...)
    end
    
    mapped_grid = F(grid)
    mesh = zeros(length(mapped_grid), size(mapped_grid[1])...)
    for k=1:length(mapped_grid)
        mesh[k,:,:,:] = mapped_grid[k]
    end
    vtkmodel = vtk_grid(filename, mesh)

    ∇F = jacobian(F)    
    J = sqrt.(geometric_factor(∇F(grid)))

    Postprocessor(vtkmodel, trialspace, J, grid)
end

function process_results!(postprocessor, lambda, v, modes)
    for k in modes
        postprocessor.trialspace.coeffs[:] = v[:,k]
        vtk_add_field!(
            postprocessor.vtkmodel,
            string("#", k, "; λ= ", lambda[k]),
            postprocessor.trialspace(postprocessor.grid) ./ postprocessor.J
        )
    end
end

function process_galerkin_results!(postprocessor, lambda, v, modes)
    for k in modes
        postprocessor.trialspace.coeffs[:] = v[:,k]
        vtk_add_field!(
            postprocessor.vtkmodel,
            string("#", k, "; λ= ", lambda[k]),
            postprocessor.trialspace(postprocessor.grid) 
        )
    end
end

function vtk_add_field!(vtkmodel, fieldname::String, fielddata)
    vtkmodel[fieldname] = fielddata
end

save_results(postprocessor::Postprocessor) = vtk_save(postprocessor.vtkmodel)