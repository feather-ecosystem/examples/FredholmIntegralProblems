export LinearOperator, mul!

struct LinearOperator{I,T,D} <: LinearMaps.LinearMap{T} 
    K::KernelOperator{I,T,D}
    size::Dims{2}
    Zₗ
    Zᵤ
    M
    B̃ᵤ
    B̃ₗ
    J
    function LinearOperator(K::KernelOperator{I,T,D}, Zₗ, Zᵤ, M, B̃ᵤ, B̃ₗ, J) where {I,T,D}
        return new{I,T,D}(K, size(Zₗ), Zₗ, Zᵤ, M, B̃ᵤ, B̃ₗ, J)
    end
end

Base.size(L::LinearOperator) = L.size

function LinearAlgebra.mul!(b::AbstractVecOrMat, op::LinearOperator, v::AbstractVector)
    LinearMaps.check_dim_mul(b, op, v)

    # Alg. 1
    v = op.Zᵤ \ v                                   # stage 1
    ṽ = op.M * v                                    # stage 2
    ṽ = op.B̃ᵤ' \ (op.B̃ₗ' \ ṽ)                        # stage 3
    ṽ .*= view(op.J,:)                              # stage 4
    y = FredholmIntegralProblems.mul(op.K, ṽ)    # stage 5
    ṽ = y .* view(op.J,:)                        # stage 6
    ṽ = op.B̃ₗ \ (op.B̃ᵤ \ ṽ)                          # stage 7
    v = op.M' * ṽ                                   # stage 8
    b .= op.Zₗ \ v                                   # stage 9
end

LinearAlgebra.issymmetric(::LinearOperator) = true
LinearAlgebra.isposdef(::LinearOperator) = true
LinearAlgebra.ishermitian(::LinearOperator) = true