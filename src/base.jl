export @evaluate!, geometric_factor, geometric_factor!, grevillepoints_singularityfix, stiffshell

using Base.Cartesian
import FeaInterfaces.evalkernel!

for Dim in 5:6
    @eval @inline function Base.getindex(X::CartesianProduct{$Dim}, i::Vararg{Int, $Dim})
        return ntuple(k -> X.data[k][i[k]], $Dim)
    end
end

for op in [:(=), :(+=), :(-=), :(*=), :(/=)]
    local S = Val{op}

    # 5d to 6d with Cartesian product points
    for Dim in 5:6
        local kernel_expression = Expr(op, :(@nref $Dim Y i), :(@ncall $Dim func x))
        @eval function evalkernel!(::$S, Y::AbstractArray{T,$Dim}, X, func::Function) where T
            @nloops $Dim i Y d -> x_d = X[d][i_d] begin
                $kernel_expression
            end
            return Y
        end
    end
end

function geometric_factor!(detJ, J::Array{Array{T,3},2}) where {T}
    detJ  .= @. J[1,1] * J[2,2] * J[3,3]
    @. detJ += J[1,2] * J[2,3] * J[3,1]  
    @. detJ += J[1,3] * J[2,1] * J[3,2]  
    @. detJ -= J[1,3] * J[2,2] * J[3,1]  
    @. detJ -= J[1,2] * J[2,1] * J[3,3]  
    @. detJ -= J[1,1] * J[2,3] * J[3,2]  
end

function geometric_factor!(detJ, J::Array{Array{T,2},2}) where {T}
    detJ .= @. J[1,1] * J[2,2] - J[1,2] * J[2,1]
end

function geometric_factor!(detJ, J::Array{Array{T,1},2}) where {T}
    detJ .= J[1,1]
end

function geometric_factor(J::Array{Array{T,3},2}) where {T}
    detJ  = @. J[1,1] * J[2,2] * J[3,3]
    @. detJ += J[1,2] * J[2,3] * J[3,1]  
    @. detJ += J[1,3] * J[2,1] * J[3,2]  
    @. detJ -= J[1,3] * J[2,2] * J[3,1]  
    @. detJ -= J[1,2] * J[2,1] * J[3,3]  
    @. detJ -= J[1,1] * J[2,3] * J[3,2]  
end

function geometric_factor(J::Array{Array{T,2},2}) where {T}
    detJ = @. J[1,1] * J[2,2] - J[1,2] * J[2,1]
end

function geometric_factor(J::Array{Array{T,1},2}) where {T}
    detJ .= J[1,1]
end

function grevillepoints_singularityfix(g::IncreasingVector{T}) where T
    vals, m = deconstruct_vector(g)

    ii = 1
    eps = 1e-12
    G = Vector{T}(undef, sum(m))
    for (val,count) in zip(vals, m)
        if count == 2
            G[ii] = val - eps; ii += 1
            G[ii] = val + eps; ii += 1
        else
            G[ii] = val; ii += 1
        end
    end
    return IncreasingVector(G)
end

function stiffshell()
    p = Degree(2)

    U₁ = KnotVector([0, 0, 0, 1.5707963267949, 1.5707963267949, 3.14159265358979, 3.14159265358979, 4.71238898038469, 4.71238898038469, 6.28318530717959, 6.28318530717959,  6.28318530717959])
    U₂ = KnotVector([-88.6003574854838,-88.6003574854838,-88.6003574854838,-2,-2,-1,-1,0,0,0] .+ 88.6003574854838)
    U₃ = KnotVector([0.,0.,0.,1.,1.,1.])
    
    U₁ = U₁ / U₁[end]
    U₂ = U₂ / U₂[end]
    U₃ = U₃ / U₃[end]

    S₁ = SplineSpace(p, U₁)
    S₂ = SplineSpace(p, U₂)
    S₃ = SplineSpace(p, U₃)

    volume = Mapping(Nurbs, S₁ ⨷ S₂ ⨷ S₃; codimension=3)
    
    inner = readdlm("examples/data/shell_inner.csv", '\t', Float64, '\n')
    center = readdlm("examples/data/shell_center.csv", '\t', Float64, '\n')
    outer = readdlm("examples/data/shell_outer.csv", '\t', Float64, '\n')
    
    surfs = [outer, center, inner] 
    shape = (9,7,3)
    
    xcoeffs, ycoeffs, zcoeffs, weights = [], [], [], []
    for surf in surfs
        push!(xcoeffs, surf[:,1]...) 
        push!(ycoeffs, surf[:,2]...) 
        push!(zcoeffs, surf[:,3]...) 
        push!(weights, surf[:,4]...) 
    end

    volume.weights[:] .= weights
    volume[1].coeffs[:] .= xcoeffs
    volume[2].coeffs[:] .= ycoeffs
    volume[3].coeffs[:] .= zcoeffs

    return volume
end